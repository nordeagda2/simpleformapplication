TREŚĆ:
Należy napisać aplikację (wykorzystując załączony kod) która w pętli while czyta ze Scannera wejście użytkownika z konsoli, a następnie linia po linii wypisuje tekst do pliku. Aplikacja ma się zamykać po wpisaniu przez użytkownika komendy "quit".

WERSJA_BAZOWA:
    
    public class Main {
    public static void main(String[] args) {
    
        System.out.println("Zaczynamy.");
        
        File naszPlik = new File("test.txt");
        
        if (naszPlik.exists()) {
        
            System.out.println("Plik istnieje.");
            
        } else {
        
            System.out.println("Plik nie istnieje.");
            
        }
        
    // pawel.reclaw@gmail.com
    
        if (naszPlik.isFile()) {
        
            try(FileOutputStream fos = new FileOutputStream(naszPlik)){
            
                PrintWriter writer = new PrintWriter(fos);
                
                writer.println("plik");
                
                writer.close();
                
            }catch (IOException fnfe){
            
                System.out.println("Exception : " + fnfe.getMessage());
                
            }
            
        }
        
    }
    
    }

